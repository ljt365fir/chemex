<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StaffRecordsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('staff_records')->delete();


    }
}
